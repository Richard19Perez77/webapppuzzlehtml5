var canvas;
var context;
var sectionWidth;
var sectionHeight;
var mouseX = 0;
var mouseY = 0;
var movingPiece = false;
var currentMovingFromPiece = -1;
var currentMovingToPiece = 0;
var offsetX = 0;
var offsetY = 0;
var imageObj;
var solved = false;
var currentImageNumber = 0;
var imageObj = new Image();
var PIECES = 16;
var SIDES = 4;
var isAnimating = false;

var IMAGECOUNT = 96;

var artTitle = new Array();
artTitle[0] = 'miku (1).jpg';
artTitle[1] = 'miku (2).jpg';
artTitle[2] = 'miku (3).jpg';
artTitle[3] = 'miku (4).jpg';
artTitle[4] = 'miku (5).jpg';
artTitle[5] = 'miku (6).jpg';
artTitle[6] = 'miku (7).jpg';
artTitle[7] = 'miku (8).jpg';
artTitle[8] = 'miku (9).jpg';
artTitle[9] = 'miku (10).jpg';
artTitle[10] = 'miku (11).jpg';
artTitle[11] = 'miku (12).jpg';
artTitle[12] = 'miku (13).jpg';
artTitle[13] = 'miku (14).jpg';
artTitle[14] = 'miku (15).jpg';
artTitle[15] = 'miku (16).jpg';
artTitle[16] = 'miku (17).jpg';
artTitle[17] = 'miku (18).jpg';
artTitle[18] = 'miku (19).jpg';
artTitle[19] = 'miku (20).jpg';
artTitle[20] = 'miku (21).jpg';
artTitle[21] = 'miku (22).jpg';
artTitle[22] = 'miku (23).jpg';
artTitle[23] = 'miku (24).jpg';
artTitle[24] = 'miku (25).jpg';
artTitle[25] = 'miku (26).jpg';
artTitle[26] = 'miku (27).jpg';
artTitle[27] = 'miku (28).jpg';
artTitle[28] = 'miku (29).jpg';
artTitle[29] = 'miku (30).jpg';
artTitle[30] = 'miku (31).jpg';
artTitle[31] = 'miku (32).jpg';
artTitle[32] = 'miku (33).jpg';
artTitle[33] = 'miku (34).jpg';
artTitle[34] = 'miku (35).jpg';
artTitle[35] = 'miku (36).jpg';
artTitle[36] = 'miku (37).jpg';
artTitle[37] = 'miku (38).jpg';
artTitle[38] = 'miku (39).jpg';
artTitle[39] = 'miku (40).jpg';
artTitle[40] = 'miku (41).jpg';
artTitle[41] = 'miku (42).jpg';
artTitle[42] = 'miku (43).jpg';
artTitle[43] = 'miku (44).jpg';
artTitle[44] = 'miku (45).jpg';
artTitle[45] = 'miku (46).jpg';
artTitle[46] = 'miku (47).jpg';
artTitle[47] = 'miku (48).jpg';
artTitle[48] = 'miku (49).jpg';
artTitle[49] = 'miku (50).jpg';
artTitle[50] = 'miku (51).jpg';
artTitle[51] = 'miku (52).jpg';
artTitle[52] = 'miku (53).jpg';
artTitle[53] = 'miku (54).jpg';
artTitle[54] = 'miku (55).jpg';
artTitle[55] = 'miku (56).jpg';
artTitle[56] = 'miku (57).jpg';
artTitle[57] = 'miku (58).jpg';
artTitle[58] = 'miku (59).jpg';
artTitle[59] = 'miku (60).jpg';
artTitle[60] = 'miku (61).jpg';
artTitle[61] = 'miku (62).jpg';
artTitle[62] = 'miku (63).jpg';
artTitle[63] = 'miku (64).jpg';
artTitle[64] = 'miku (65).jpg';
artTitle[65] = 'miku (66).jpg';
artTitle[66] = 'miku (67).jpg';
artTitle[67] = 'miku (68).jpg';
artTitle[68] = 'miku (69).jpg';
artTitle[69] = 'miku (70).jpg';
artTitle[70] = 'miku (71).jpg';
artTitle[71] = 'miku (72).jpg';
artTitle[72] = 'miku (73).jpg';
artTitle[73] = 'miku (74).jpg';
artTitle[74] = 'miku (75).jpg';
artTitle[75] = 'miku (76).jpg';
artTitle[76] = 'miku (77).jpg';
artTitle[77] = 'miku (78).jpg';
artTitle[78] = 'miku (79).jpg';
artTitle[79] = 'miku (80).jpg';
artTitle[80] = 'miku (81).jpg';
artTitle[81] = 'miku (82).jpg';
artTitle[82] = 'miku (83).jpg';
artTitle[83] = 'miku (84).jpg';
artTitle[84] = 'miku (85).jpg';
artTitle[85] = 'miku (86).jpg';
artTitle[86] = 'miku (87).jpg';
artTitle[87] = 'miku (88).jpg';
artTitle[88] = 'miku (89).jpg';
artTitle[89] = 'miku (90).jpg';
artTitle[90] = 'miku (91).jpg';
artTitle[91] = 'miku (92).jpg';
artTitle[92] = 'miku (93).jpg';
artTitle[93] = 'miku (94).jpg';
artTitle[94] = 'miku (95).jpg';
artTitle[95] = 'miku (96).jpg';
artTitle[96] = 'miku (97).jpg';

function Piece() {
	var pieceNumber;
	var x;
	var y;
}

var pieceArray = new Array();

function Slot() {
	var slotNumber;
	var x;
	var y;
	var Piece;
}

var slotArray = new Array();

// 720 x 450 image for canvas

function init(document) {
	canvas = document.getElementById('puzzleCanvas');
	currentImageNumber = Math.floor(Math.random() * IMAGECOUNT);
	var canvasOffset = $("#puzzleCanvas").offset();
	offsetX = Math.round(canvasOffset.left);
	offsetY = Math.round(canvasOffset.top);
	sectionWidth = canvas.width / SIDES;
	sectionHeight = canvas.height / SIDES;
	canvas.addEventListener("mousedown", doMouseDown, false);
	canvas.addEventListener("mouseup", doMouseUp, false);
	canvas.addEventListener("mousemove", doMouseMove, false);
	canvas.addEventListener("mouseout", doMouseOut, false);
	context = canvas.getContext('2d');

	document.getElementById("savePuzzleButton").addEventListener("click",
			function() {
				// creating a literal object "progress" filled with values from
				// inputs
				var progress = {
					currentImageNumber : currentImageNumber,
					pieces : PIECES,
					sides : SIDES,
					slotArray : slotArray,
					pieceArray : pieceArray
				};

				localStorage["progress"] = JSON.stringify(progress);

			});

	document.getElementById("loadPuzzleButton").addEventListener("click",
			function() {
				// decoding "progress" from local storage
				$('#buttonDiv').finish();
				var progress = JSON.parse(localStorage.getItem("progress"));
				currentImageNumber = progress.currentImageNumber;
				slotArray = progress.slotArray;
				pieceArray = progress.pieceArray;
				PIECES = progress.pieces;
				SIDES = progress.sides;
				sectionWidth = canvas.width / SIDES;
				sectionHeight = canvas.height / SIDES;
				solved = false;
				loadPreviousImage();
			});

	document.getElementById("nextImageButton").addEventListener("click",
			function() {
				$('#buttonDiv').finish();
				currentImageNumber++;
				if (currentImageNumber > IMAGECOUNT) {
					currentImageNumber = 0;
				}
				initArrays();
				loadImage();
			});

	document.getElementById("changeDifficultyButton").addEventListener("click",
			function() {
				$('#buttonDiv').finish();
				if (SIDES > 5) {
					SIDES = 2;
					PIECES = SIDES * SIDES;
				} else {
					SIDES++;
					PIECES = SIDES * SIDES;
				}

				sectionWidth = canvas.width / SIDES;
				sectionHeight = canvas.height / SIDES;
				initArrays();
				loadImage();
			});
	initArrays();
	loadImage();
}

function clearCanvas() {
	context.save();

	// Use the identity matrix while clearing the canvas
	context.setTransform(1, 0, 0, 1, 0, 0);
	context.clearRect(0, 0, canvas.width, canvas.height);

	// Restore the transform
	context.restore();
	context.fillStyle = "#A1D0C7";
	context.fillRect(0, 0, canvas.width, canvas.height);
}

function initArrays() {
	pieceArray = new Array();
	slotArray = new Array();
	for (i = 0; i < PIECES; i++) {
		pieceArray[i] = new Piece();
		pieceArray[i].pieceNumber = i;
		slotArray[i] = new Slot();
		slotArray[i].slotNumber = i;
		slotArray[i].piece = pieceArray[i];
	}

	for (y = 0; y < SIDES; y++) {
		for (x = 0; x < SIDES; x++) {
			var index = x + (y * SIDES);
			var w = x * sectionWidth;
			var h = y * sectionHeight;
			slotArray[index].x = w;
			slotArray[index].y = h;
			pieceArray[index].x = w;
			pieceArray[index].y = h;
		}
	}
}

function adjustOffset() {
	var canvasOffset = $("#puzzleCanvas").offset();
	offsetX = Math.round(canvasOffset.left);
	offsetY = Math.round(canvasOffset.top);
}

function loadPreviousImage() {
	isAnimating = true;
	$('#buttonDiv').slideUp();
	imageObj.src = 'images/' + artTitle[currentImageNumber];
	imageObj.onload = function() {
		$('#buttonDiv').slideDown(400, function() {
			isAnimating = false;
			checkForAllPiecesInCorrectSlot();
			drawCanvas();
		});
	}
}

function loadImage() {
	isAnimating = true;
	solved = false;
	$('#buttonDiv').slideUp();
	imageObj.src = 'images/' + artTitle[currentImageNumber];
	imageObj.onload = function() {
		putPiecesInRandomSlots();
		$('#buttonDiv').slideDown(400, function() {
			isAnimating = false;
			checkForAllPiecesInCorrectSlot();
			drawCanvas();
		});
	}
}

function doMouseDown(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		currentMovingFromPiece = getCurrentPiece(mouseX, mouseY);
		drawCanvas();
	}
}

function doMouseMove(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		drawCanvas();
	}
}

function doMouseUp(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		currentMovingToPiece = getCurrentPiece(mouseX, mouseY);
		switchPieces(currentMovingFromPiece, currentMovingToPiece);
		currentMovingFromPiece = -1;
		checkForAllPiecesInCorrectSlot();
		drawCanvas();
	}
}

function checkForAllPiecesInCorrectSlot() {
	var piecesSolved = 0;
	for (var int = 0; int < PIECES; int++) {
		if (slotArray[int].slotNumber == slotArray[int].piece.pieceNumber) {
			piecesSolved++;
		}
	}
	if (piecesSolved == PIECES) {
		solved = true;
	}
}

function doMouseOut(event) {
	currentMovingFromPiece = -1;
	drawCanvas();
}

function getCurrentPiece(x, y) {
	var xIndex = 0;
	var tempSectionWidth = sectionWidth;
	while (x >= tempSectionWidth) {
		xIndex++;
		tempSectionWidth += sectionWidth;
	}

	var yIndex = 0;
	var tempSectionHeight = sectionHeight;
	while (y >= tempSectionHeight) {
		yIndex++;
		tempSectionHeight += sectionHeight;
	}

	var pieceIndex = xIndex + (yIndex * SIDES);

	return pieceIndex;
}

function putPiecesInRandomSlots() {
	var isRandom = false;
	var attempts = 0;
	var moves = 0;
	if (PIECES % 2 == 0) {
		moves = PIECES / 2;
	} else {
		moves = (PIECES / 2) + 1;
	}
	while (!isRandom) {
		attempts++;
		for (var from = 0; from < moves; from++) {
			var to = Math.floor(Math.random() * PIECES);
			while (from == to) {
				to = Math.floor(Math.random() * PIECES);
			}
			switchPieces(from, to);
		}

		isRandom = true;
		for (var int = 0; int < slotArray.length; int++) {
			if (slotArray[int].slotNumber == slotArray[int].piece.pieceNumber) {
				isRandom = false;
			}
		}
	}
}

function switchPieces(fromSlot, toSlot) {
	var tempPiece = slotArray[toSlot].piece;
	slotArray[toSlot].piece = slotArray[fromSlot].piece;
	slotArray[fromSlot].piece = tempPiece;
}

function drawCanvas() {
	// Store the current transformation matrix
	clearCanvas();
	drawPieces();
}

function drawBorder() {
	context.strokeStyle = "#000000";
	context.lineWidth = 3;
	context.beginPath();

	var tempWidth;
	var tempHeight;
	for (var int = 1; int <= SIDES - 1; int++) {
		tempWidth = sectionWidth * int;

		context.moveTo(tempWidth, 0);
		context.lineTo(tempWidth, 0);
		context.lineTo(tempWidth, canvas.height);

		tempHeight = sectionHeight * int;

		context.moveTo(0, tempHeight);
		context.lineTo(0, tempHeight);
		context.lineTo(canvas.width, tempHeight);
	}

	context.stroke();
}

function drawPieces() {
	for (var int = 0; int < PIECES; int++) {
		var slot = slotArray[int];
		var piece = slotArray[int].piece;
		var movingPiece;
		if (currentMovingFromPiece != int) {
			context.drawImage(imageObj, piece.x, piece.y, sectionWidth,
					sectionHeight, slot.x, slot.y, sectionWidth, sectionHeight);
		} else {
			movingPiece = piece;
		}

		if (solved == false) {
			drawBorder();
		}

		if (movingPiece != null) {
			var centerX = mouseX - sectionWidth / 2;
			var centerY = mouseY - sectionHeight / 2;
			context.drawImage(imageObj, movingPiece.x, movingPiece.y,
					sectionWidth, sectionHeight, centerX, centerY,
					sectionWidth, sectionHeight);
		}
	}
}